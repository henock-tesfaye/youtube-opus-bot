import json
import random
import time
from operator import itemgetter

import telepot

bot_json_path = '/home/jupi/Talks/Bots/BibleBot/%s.json'
all_references = json.load(file(bot_json_path % 'all_references'))
bible = json.load(file(bot_json_path % 'bible'))


def find_verse(book_index, chapter_index, start, end):
    book = bible[book_index - 1]
    chapters = book['chapters']
    chapter = chapters[chapter_index - 1]
    verses_for_chapter = chapter['verses']
    verse = verses_for_chapter[start - 2:end]
    text = ' '.join(map(itemgetter('text'), verse))
    verse_string = "%s:%s-%s" % (
        chapter_index, start, ['', end][start == end]
    )
    return "{}\n\n{}\n\n{}".format(book['name'], verse_string.strip('-'), text)


def handle(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)

    if content_type == 'text':
        bot.sendMessage(chat_id,
                        find_verse(*random.choice(all_references)))


TOKEN = "371673553:AAF20hIi7Y5uxHEYVvrY1dVNb0ubIDnHH6o"

bot = telepot.Bot(TOKEN)

bot.message_loop(handle)
print('Listening ...')

# Keep the program running.
while 1:
    time.sleep(10)
