#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pymemcache.client import base


class DictMethodsMixin(object):
    def setdefault(self, key, default=None):
        result = self.get(key)
        if result is None:
            self.set(key, default)
            return default
        return result

    def pop(self, key):
        value = self.get(key)
        self.delete(key)
        return value


class DictLikeClient(DictMethodsMixin, base.Client):
    pass
