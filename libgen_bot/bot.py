#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import operator as op

from emoji import emojize
from libgen import MirrorFinder
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from telegram.ext import CallbackQueryHandler, Filters, MessageHandler, Updater

WHITE_LIST = ['thefieldmarshal', 'pewterarm', 'Ahadu_X', 'AdeyTes', 'Leykb']
FLOPPY = emojize(":floppy_disk:", use_aliases=True)
BOOK_TEMPLATE = """
<b>Title </b>: {0.title}

<b>Authors </b>: {0.authors}

<b>Extension </b>: {0.extension}

<b>Year </b>: {0.year}

<b>Size </b>: {0.size}
"""

global_books = {}

ActiveMirror = MirrorFinder().find_active_mirror()

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.ERROR,
)

logger = logging.getLogger(__name__)


def get_books(search_term):
    active_mirror = ActiveMirror(search_term)
    for result_page in active_mirror.search(search_term):
        for book in active_mirror.extract(result_page)[::-1]:
            global_books[book.id] = {"mirror": active_mirror, "book": book}
            yield book
        break


def download(update, context):
    book_id = update.callback_query.data
    mirror, book = op.itemgetter("mirror", "book")(
        global_books.get(book_id)
    )
    filename = mirror.download(book)
    update.callback_query.message.reply_document(open(filename, "rb"))


def search(update, context):
    if update.message.from_user.username not in WHITE_LIST:
        update.message.reply_text(
            "Sorry, only {} are allowed to use this bot".format(
                ', '.join(WHITE_LIST)
            )
        )
    search_term = update.message.text
    update.message.reply_text("Searching …")
    books = get_books(search_term)
    for book in books:
        update.message.reply_text(
            BOOK_TEMPLATE.format(book),
            parse_mode=ParseMode.HTML,
            reply_markup=InlineKeyboardMarkup([[
                InlineKeyboardButton("Download %s" % FLOPPY,
                                     callback_data=book.id)
            ]]),
        )


TOKEN = "943443498:AAE5UGKD3NhIK3dhdGtDvM7DHaypP-2ICXc"
updater = Updater(TOKEN, use_context=True)
updater.dispatcher.add_handler(MessageHandler(Filters.text, search))
updater.dispatcher.add_handler(CallbackQueryHandler(download))
updater.start_polling()
updater.idle()
