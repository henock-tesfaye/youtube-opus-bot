#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import pprint
from collections import defaultdict

from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    InlineQueryResultArticle,
    ParseMode,
)
from telegram.ext import (
    CallbackQueryHandler,
    CommandHandler,
    Filters,
    InlineQueryHandler,
    MessageHandler,
    Updater,
)

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

logger = logging.getLogger(__name__)


def send_explanation(update, context):
    update.message.reply_text(
        "Welcome to Survey bots\n"
        "You can create a survey by /create_survey\n"
        "You can list your existing surveys by /list_surveys\n"
        "You can export the result of the survey by /export_survey <uuid>\n"
    )


def inline_send_survey_list(update, context):
    update.query.text


def send_surveys_list(update, context):
    update.message.text


def export(udpate, context):
    pass


def error_handler(update, context):
    print(update)
    print(context)
    print(update.error)


# decorators mnamn. A proper framework for handling states

states = [
    "start",
    "title",
    # "description",
    # States that go one back? Stack?
    # "instructions",
    # States that go one back? Stack?
    "first_question",
    "next_question",
    "first_choice",
    "next_choice",
]


state_storage = defaultdict(
    lambda: "start",
    {
        # chat id : state
    },
)

state_messages = {
    "start": None,
    "title": "Please provide the title of the survey to get things started",
    "description": "Give us the description of the survey",
    "instructions": "Give us the instructions",
    "first_question": "Give us the first question",
    "next_question": "Give us the next question",
    "first_choice": "Give us the next question",
    "next_choice": "Give us the next choice",
}

survey = {
    "title": "pending",
    "description": "pending",
    "instructions": "pending",
    "questions": [],
}


def get_current_state(chat_id):
    return state_storage[chat_id]


def get_next_state(chat_id):
    current_state = get_current_state(chat_id)
    try:
        return states[states.index(current_state) + 1]
    except IndexError:
        return "???"


def advance_state(chat_id):
    next_state = get_next_state(chat_id)
    state_storage[chat_id] = next_state
    return next_state


def set_state(chat_id, state):
    state_storage[chat_id] = state
    return state


def represent_survey(update):
    message = (
        "<b>Survey</b>: {title}\n\n"
        "<b>Description</b>: {description}\n\n"
        "<b>Instructions</b>: {instructions}"
    ).format(**survey)

    keyboard_markup = InlineKeyboardMarkup(
        [
            [
                InlineKeyboardButton(
                    "{verb} description".format(
                        verb=(
                            "Add" if survey.get("description") == "pending" else "Edit"
                        )
                    ),
                    callback_data="description",
                )
            ],
            [
                InlineKeyboardButton(
                    "{verb} instructions".format(
                        verb=(
                            "Add" if survey.get("instructions") == "pending" else "Edit"
                        )
                    ),
                    callback_data="instructions",
                )
            ],
            [
                InlineKeyboardButton(
                    "Add questions",
                    callback_data=(
                        "first_question"
                        if "first_question" not in survey
                        else "next_question"
                    ),
                )
            ],
            [InlineKeyboardButton("Delete", callback_data="Delete")],
        ]
    )
    return update.message.reply_text(
        message, reply_markup=keyboard_markup, parse_mode=ParseMode.HTML
    )


def create_survey(update, context):
    next_state = get_next_state(update.message.chat_id)
    message = state_messages[next_state]
    update.message.reply_text(message)
    advance_state(update.message.chat_id)

    # update.message.reply_text("Here is the permalink."
    #                           "Give it to the people you want to fill "
    #                           "this survey")


# Ability to edit question. Saving text only when finalyzing the survey


def converse(update, context):
    chat_id = update.message.chat_id
    state = get_current_state(chat_id)
    if state == "start":
        return update.message.reply_text("To create a survey try /create_survey")
    else:
        if state == "title":
            survey[state] = update.message.text
            represent_survey(update)
        elif state in ["description", "instructions"]:
            survey[state] = update.message.text
            # next_state = get_next_state(chat_id)
            represent_survey(update)
        elif state in ["first_question", "next_question"]:
            question = update.message.text
            survey["questions"].append(question)
            update.message.reply_text(
                "<b>Question</b> : {}".format(question),
                reply_markup=InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "Edit question", callback_data="edit_question"
                            )
                        ],
                        [
                            InlineKeyboardButton(
                                "Add another question", callback_data="next_question"
                            )
                        ],
                    ]
                ),
                parse_mode=ParseMode.HTML,
            )
        else:
            pass

    # next_state = advance_state(update.message.chat_id)
    # message = state_messages[next_state]
    # update.message.reply_text(message)

    pprint.pprint(survey)


def callback_handler(update, context):
    state = update.callback_query.data
    update.callback_query.answer(state)
    message = state_messages[state]
    update.callback_query.message.reply_text(message)
    set_state(update.callback_query.message.chat_id, state)

    # Description and instructions aren't states


updater = Updater("980010497:AAE8ol9zpS0tc41lvSja0ZkSLT1S1acQQV8", use_context=True)
dp = updater.dispatcher
dp.add_handler(CommandHandler("start", send_explanation))
dp.add_handler(CommandHandler("help", send_explanation))
dp.add_handler(CommandHandler("create_survey", create_survey))
dp.add_handler(CommandHandler("list_surveys", send_surveys_list))
dp.add_handler(CommandHandler("export", export))
dp.add_handler(MessageHandler(Filters.text, converse))
dp.add_handler(InlineQueryHandler(inline_send_survey_list))
dp.add_handler(CallbackQueryHandler(callback_handler))
dp.add_error_handler(error_handler)

updater.start_polling()
updater.idle()
