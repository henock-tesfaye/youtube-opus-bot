#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import re
from uuid import uuid4

from peewee import OperationalError

from database import Entry, EntryIndex
from emoji import emojize
from telegram import (InlineKeyboardButton, InlineKeyboardMarkup,
                      InlineQueryResultArticle, InputTextMessageContent,
                      ParseMode)
from telegram.ext import (CallbackQueryHandler, CommandHandler, Filters,
                          InlineQueryHandler, MessageHandler, Updater)

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.ERROR,
)

logger = logging.getLogger(__name__)

LIMIT = 5

STAR = emojize(":star:", use_aliases=True)
FLOPPY = emojize(":floppy_disk:", use_aliases=True)
PAPERCLIP = emojize(":paperclip:", use_aliases=True)
LINK = emojize(":link:", use_aliases=True)

FORMAT = """<b>{entry.term}</b> ({entry.type}) {icon}

{entry.definition}

<a href="https://telegram.me/etymo_bot?start={entry.id}">{link_icon}</a>
"""

URL_TEMPLATE = r'<a href="https://telegram.me/etymo_bot?start=\1">\1</a>'


def pango_to_markdown(pango):
    patterns_to_bolden = [
        r'<span weight=\'bold\'>([^<>]*?)</span>',
        r'<span style=\'italic\' weight=\'bold\'>([^<>]*?)</span>'
    ]
    for pattern in patterns_to_bolden:
        pango = re.sub(pattern, r'<b>\1</b>', pango)

    for pattern in [r'<\w{2,} ?.*?>', r'</\w{2,}>']:
        pango = re.sub(pattern, r'', pango)

    return pango


def replace_crossreferences(entry):
    for crossreference in map(re.escape, entry.crossreferences):
        entry.definition = re.sub(r'\b(%s)\b' % crossreference,
                                  URL_TEMPLATE,
                                  entry.definition)
    return entry.definition


def represent(entry):
    entry.definition = replace_crossreferences(entry)
    entry.definition = pango_to_markdown(entry.definition)
    icon = STAR if entry.favorite else ''
    return FORMAT.format(entry=entry, icon=icon, link_icon=LINK)


def get_matches(search_term):
    if search_term.startswith("r'"):
        search_term = search_term[2:]
        matches = (
            Entry.select().where(Entry.term.regexp(search_term)).limit(LIMIT)
        )
    else:
        matches = EntryIndex.search(
            search_term,
            weights={"term": 2, "definition": 1},
            with_score=True,
            score_alias="search_score",
        ).limit(LIMIT)
    try:
        return list(matches)
    except OperationalError:
        return get_matches("r'^({})$".format(
            '|'.join([search_term, search_term.lower()])
        ))


def insert_crossreferences(event_box, data, crossreferences=[]):
    if not crossreferences:
        return
    crossreferences = [term.replace("*", "") for term in crossreferences]
    crossreferences = [
        re.sub(r"^-(.*?)$", r"\1$", term) for term in crossreferences
    ]
    crossreferences = [
        re.sub(r"(.*?)-$", r"^\1", term) for term in crossreferences
    ]
    search_term = "|".join(crossreferences)
    search_term = "r'%s" % search_term


def load_favorites(update, context):
    favorite_entries = (
        Entry.select().where(Entry.favorite == True).limit(LIMIT)
    )
    for favorite_entry in favorite_entries:
        update.message.reply_text(represent(favorite_entry),
                                  **entry_to_message_dict(favorite_entry))


def entry_to_message_dict(entry):
    return {
        'parse_mode': ParseMode.HTML,
        'reply_markup': InlineKeyboardMarkup([
            [InlineKeyboardButton(
                ('Remove' if entry.favorite else 'Add') + ' ' + STAR,
                callback_data=entry.id)],
            [InlineKeyboardButton(
                PAPERCLIP,
                switch_inline_query_current_chat='Related - %s: ' % entry.id)]
        ])
    }


def search(update, context):
    try:
        matches = get_matches(context.args[0])
    except (AttributeError, IndexError, TypeError):
        matches = get_matches(update.message.text)

    if not len(matches):
        update.message.reply_text("Sorry, no results.")

    for match in matches:
        update.message.reply_text(represent(match),
                                  **entry_to_message_dict(match))


def toggle_favor(update, context):
    entry_id = update.callback_query.data
    entry = Entry.select().where(Entry.id == entry_id)[0]
    entry.favorite = not entry.favorite
    entry.save()
    update.callback_query.answer('{term} {action} favorites'.format(
        term=entry.term,
        action='added to' if entry.favorite else 'removed from'
    ))
    update.callback_query.edit_message_text(
        text=represent(entry),
        **entry_to_message_dict(entry)
    )


def get_one_entry(update, entry_id):
    entry = Entry.select().where(Entry.id == entry_id)[0]
    update.message.reply_text(represent(entry),
                              **entry_to_message_dict(entry))


def start(update, context):
    search_item = context.args[0]
    if search_item.isdigit():
        get_one_entry(update, search_item)
    else:
        search(update, context)


def inline_search(update, context):
    search_term = update.inline_query.query
    matches = get_matches(search_term)
    results = []
    for match in matches:
        results.append(InlineQueryResultArticle(
            id=uuid4(),
            title='{entry.term} ({entry.type})'.format(entry=match),
            description=pango_to_markdown(match.definition),
            input_message_content=InputTextMessageContent(
                message_text=represent(match),
                **entry_to_message_dict(match)
            ),
        ))

    update.inline_query.answer(results)


TOKEN = "1065307490:AAFixm68NTLYwJAYWUOUYsNUSRrh3F2urww"
updater = Updater(TOKEN, use_context=True)
dp = updater.dispatcher
dp.add_handler(CommandHandler("start", start, pass_args=True))
dp.add_handler(CommandHandler("favorites", load_favorites))
dp.add_handler(MessageHandler(Filters.text, search))
dp.add_handler(InlineQueryHandler(inline_search))
dp.add_handler(CallbackQueryHandler(toggle_favor))

updater.start_polling()
updater.idle()
