#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import os
import re

import peewee as pw
from playhouse import sqlite_ext as sqx

path = os.path.dirname(os.path.abspath(__file__))

db = sqx.SqliteExtDatabase(os.path.join(path, 'etymoffline.db'),
                           regexp_function=True, timeout=3,
                           pragmas=(('journal_mode', 'wal'),))

CSS_TO_PANGO = {
    'definition': "font-family='georgia' size='12384'",
    'highlight': "weight='bold'",
    'foreign': "style='italic' weight='bold'",
    'crossreference': "weight='bold'",
    'dictionary': "color='blue' weight='bold'",
}


def to_pango(html):
    html = re.sub(r'^<dd.*?>', r'<span class="definition">', html)
    html = re.sub(r'</dd>$', r'</span>', html)
    html = re.sub(r'<(/?)(?:a|dd)([ >])', r'<\1span\2', html)
    html = re.sub(r'<blockquote>\s*(.*?)\s*</blockquote>',
                  r'\n\n<span size="x-large">“</span>'
                  r'<span size="11384">\1</span>'
                  r'<span size="x-large">”</span>\n\n',
                  html, flags=re.DOTALL)
    html = html.replace('<br>', '\n')
    html = re.sub(r'class="(\w+)"', lambda mo: CSS_TO_PANGO[mo.group(1)], html)
    html = re.sub(r'href=".*?"', "", html)
    return html


class FakeJSONField(pw.TextField):

    def db_value(self, value):
        return json.dumps(value)

    def python_value(self, value):
        return json.loads(value)


class PangoField(pw.TextField):

    def db_value(self, value):
        return to_pango(value)

    def python_value(self, value):
        return to_pango(value)


class FakeJSONSearchField(sqx.SearchField):

    def python_value(self, value):
        value = super(FakeJSONSearchField, self).python_value(value)
        return json.loads(value)


class Model(pw.Model):

    class Meta:
        database = db


class Entry(Model):
    term = pw.CharField()
    type = pw.CharField()
    favorite = pw.BooleanField(default=False)
    definition = PangoField()
    crossreferences = FakeJSONField()


class EntryIndex(sqx.FTSModel):
    rowid = sqx.RowIDField()
    id = sqx.SearchField()
    term = sqx.SearchField()
    type = sqx.SearchField()
    definition = sqx.SearchField()
    favorite = sqx.SearchField()
    crossreferences = FakeJSONSearchField()

    class Meta:
        database = db
        options = {
            'tokenize': 'porter',
            'content': Entry,
        }


def refresh_database(db):
    print('Refreshing database …', end='')
    Entry.drop_table()
    EntryIndex.drop_table()
    db.create_tables([Entry, EntryIndex])
    print('DONE')


def get_json():
    print('Getting JSON …', end='')
    dictionary_file = file(os.path.join(path, 'etymonline-html-unique.jl'))
    dictionary = map(json.loads, dictionary_file)
    for entry in dictionary:
        entry.pop('link', None)
        entry['crossreferences'] = entry.pop('cross-references', [])
    print('DONE')
    return dictionary


def load_json(json_):
    print('Loading JSON …', end='')
    # Insert rows 100 at a time.
    with db.atomic():
        for idx in range(0, len(json_), 100):
            Entry.insert_many(json_[idx:idx + 100]).execute()
    print('DONE')


def store_entries(entries):
    print('Building full text search index …', end='')
    EntryIndex.rebuild()
    EntryIndex.optimize()
    print('DONE')


db.connect()
if __name__ == "__main__":
    refresh_database(db)
    dictionary = get_json()
    load_json(dictionary)
    store_entries(Entry.select())
