#!/usr/bin/env python
# -*- coding: utf-8 -*-
import collections
import logging
from itertools import islice
from uuid import uuid4

from goodreads import client
from telegram import (
    InlineQueryResultArticle,
    InlineQueryResultPhoto,
    InputTextMessageContent,
    ParseMode,
)
from telegram.ext import (
    CommandHandler,
    Filters,
    InlineQueryHandler,
    MessageHandler,
    Updater,
)

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO,
)

logger = logging.getLogger(__name__)


try:
    # from Queue import Queue
    pass
except ImportError:
    pass
    # from queue import Queue


# One for thumb nail
# cast to book on send
TEMPLATE = u"""
<b>{0.title}</b>

by <b>{0.author}</b>

<b>{0.average_rating}</b>⭐️ from <b>{0.ratings_count}</b> ratings

<b>{0.num_pages}</b> pages long

{0.description}

<a href="{0.link}">More at Goodreads</a>
"""
# [More at Goodreads]({0.link})


class CustomGoodReadsClient(client.GoodreadsClient):
    def search_books(self, q, page=1, search_field="all"):
        resp = self.request(
            "search/index.xml",
            {"q": q, "page": page, "search[field]": search_field},
        )
        works = resp["search"]["results"]["work"]

        if type(works) == collections.OrderedDict:
            works = [works]

        for work in works:
            yield work, self.book(work["best_book"]["id"]["#text"])


gc = CustomGoodReadsClient(
    "bXDgZeBC3Pcwhqh0yPw88w", "2zfaHDD7ubZBy2iE4pBhAwEwy3BtpplfS2zM2ydOa4g"
)


def get_books(query_string, n=3):
    print("Searching: ", query_string)
    work_book_pairs = gc.search_books(query_string)
    for work, book in islice(work_book_pairs, n):
        if not hasattr(book, "title"):
            setattr(book, "title", query_string)

        try:
            book.author = work["best_book"]["author"]["name"]
        except KeyError:
            book.author = "???"

        yield book


def format_books(books):
    return list(map(TEMPLATE.format, books))


def book_result_photo(book):
    return InlineQueryResultPhoto(
        id=uuid4(),
        title=book.title,
        photo_url=book.image_url,
        thumb_url=book.image_url,
        caption=TEMPLATE.format(book).replace("<br />", "\n"),
        description="TEST",  # TEMPLATE.format(book),
        parse_mode=ParseMode.HTML,
    )


def book_result_article(book):
    return InlineQueryResultArticle(
        id=uuid4(),
        title=book.title,
        description=book.author,
        thumb_url=book.image_url,
        input_message_content=InputTextMessageContent(
            TEMPLATE.format(book).replace("<br />", "\n"),
            parse_mode=ParseMode.HTML,
        ),
    )


def start(update, context):
    update.message.reply_text("Hello")


def search(update, context):
    books = get_books(update.message.text)
    for formated_book in format_books(books):
        update.message.reply_text(formated_book, parse_mode=ParseMode.HTML)


def inline_search(update, context):
    results = []
    books = get_books(update.inline_query.query)
    results = list(map(book_result_article, books))
    print("Matches: ", len(results))
    print(update.inline_query.answer(results))


TOKEN = "360768276:AAE7EsI2j2eRHe86haS2K1mx-N3ZyVfB7qE"
updater = Updater(TOKEN, use_context=True)
dp = updater.dispatcher

dp.add_handler(CommandHandler("start", start))
dp.add_handler(MessageHandler(Filters.text, search))
dp.add_handler(InlineQueryHandler(inline_search))

updater.start_polling()
updater.idle()

# Get a feeling for the need for rest: 24 * 34
# Detecting effort
# Does it feel like guessing?
# I think it shoudl feel a lot like guessing
# Don't forget struggle time
