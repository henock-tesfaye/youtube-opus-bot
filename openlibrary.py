#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from uuid import uuid4

import requests

from telegram import (InlineQueryResultArticle, InlineQueryResultPhoto,
                      InputTextMessageContent, ParseMode)
from telegram.ext import (CommandHandler, Filters, InlineQueryHandler,
                          MessageHandler, Updater)

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO
)

logger = logging.getLogger(__name__)


try:
    # from Queue import Queue
    pass
except ImportError:
    pass
    # from queue import Queue


# One for thumb nail
# cast to book on send
TEMPLATE = u"""
<a href="{url}">{title_suggest}</a>

by <b>{authors}</b>

{description}

{cover}
"""
# [More at Goodreads]({0.link})


def keep_keys(dict_, keep):
    return {key: value for key, value in dict_.items() if key in keep}

def get_books(query_string):
    response = requests.get('https://openlibrary.org/search.json',
                            params={
                                'q': query_string.strip(),
                                'mode': 'everything',
                                'has_fulltext': True
                            }).json()
    for doc in response['docs'][:5]:
        yield keep_keys(doc, ['author_name', 'title_suggest', 'type',
                              'subject', 'first_publish_year', 'isbn', 'seed'])
        # COVER AND SEEDS and bunch of fallbcks


def get_cover(book):
    return 'cover'


def format_book(book):
    try:
        isbn = book['isbn'][0]
    except (KeyError, IndexError):
        isbn = '9781473215269'

    book['cover'] = 'http://covers.openlibrary.org/b/isbn/{}-L.jpg'.format(isbn)

    book['authors'] = ', '.join(book.get('author_name', []))
    book['url'] = 'https://openlibrary.org' + book['seed'][0]

    book['detail'] = requests.get(book['url'] + '.json').json()

    try:
        book['description'] = book['detail']['description']['value']
    except KeyError as e:
        book['description'] = ''

    book['message'] = TEMPLATE.format(**book)
    return book


def start(update, context):
    update.message.reply_text('Hello')


def search(update, context):
    print('Searching: ', update.message.text)
    for book in map(format_book, get_books(update.message.text)):
        update.message.reply_text(book['message'],
                                  parse_mode=ParseMode.HTML)


def book_result_article(book):
    return InlineQueryResultArticle(
        id=uuid4(),
        title=book['title_suggest'],
        description=book['authors'],
        thumb_url=book['cover'],
        input_message_content=InputTextMessageContent(
            book['message'],
            parse_mode=ParseMode.HTML
        )
    )


def book_result_photo(book):
    return InlineQueryResultPhoto(
        id=uuid4(),
        title=book['title_suggest'],
        thumb_url=book['cover'],
        photo_url=book['cover'],
        caption=book['message'],
        description=book['authors'],
        parse_mode=ParseMode.HTML
    )


def inline_search(update, context):
    print('Inline Searching: ', update.inline_query.query)
    books = get_books(update.inline_query.query)
    results = list(map(book_result_article, map(format_book, books)))
    print('Matches: ', len(results))
    print(update.inline_query.answer(results))

TOKEN = "1015644247:AAH9zYBDPFKBdOkq78Qz5CbSethK3HNj-IA"
updater = Updater(TOKEN, use_context=True)
dp = updater.dispatcher

dp.add_handler(CommandHandler('start', start))
dp.add_handler(MessageHandler(Filters.text, search))
dp.add_handler(InlineQueryHandler(inline_search))

updater.start_polling()
updater.idle()

# Get a feeling for the need for rest: 24 * 34
# Detecting effort
# Does it feel like guessing?
# I think it shoudl feel a lot like guessing
# Don't forget struggle time
