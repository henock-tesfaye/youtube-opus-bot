#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re
import subprocess
import time
from datetime import timedelta

import youtube_dl
from pymemcache import serde
from youtube_dl.utils import sanitize_filename

from cache import DictLikeClient
from telegram import ParseMode, TelegramError
from tinydb import Query, TinyDB
from tinytag import TinyTag

client = DictLikeClient(('localhost', 11211),
                        serializer=serde.python_memcache_serializer,
                        deserializer=serde.python_memcache_deserializer)

OUTPUT_TEMPLATE = "%(title)s.%(ext)s"
OPUS_OUTPUT_TEMPLATE = "%(title)s.opus"
DEFAULT_BITRATE = 16

db = TinyDB(os.path.join(os.environ.get('HOME'), '.youtube-opus-bot.json'))


def notify_download_progress(update):
    def inner(d):
        if d["status"] == "downloading" and d['downloaded_bytes'] == 1024:
            message_text = "%(filename)s - Download started" % d
            message = update.message.reply_text(message_text)
            client.set(str(update.message.chat_id), message)
        elif d["status"] == "finished":
            message_text = "%(filename)s - Download finished" % d
            try:
                previous_message = client.pop(str(update.message.chat_id))
                message = previous_message.edit_text(message_text)
            except AttributeError:
                message = update.message.reply_text(message_text)
            client.set(str(update.message.chat_id), message)
    return inner


def fix_title(info):
    info['title'] = sanitize_filename(info['title'])
    return info


def download_youtube_video(update):
    ydl = youtube_dl.YoutubeDL(
        {
            "format": "bestaudio/best",
            "outtmpl": OUTPUT_TEMPLATE,
            "progress_hooks": [notify_download_progress(update)],
        }
    )

    with ydl:
        info = ydl.extract_info(update.message.text)

    if 'entries' in info:
        info_list = info['entries']
    else:
        info_list = [info]

    return map(fix_title, info_list)


def update_conversion_progress(process, update, info):
    progress_message_template = (
        "Converting\n\n"
        "{opus_filename} : {current_duration} / {duration}"
    )

    duration = timedelta(seconds=info["duration"])

    opus_filename = OPUS_OUTPUT_TEMPLATE % info

    previous_message = client.pop(str(update.message.chat_id))

    progress_message = previous_message.edit_text('Conversion started')

    while process.poll() is None:
        time.sleep(5)
        try:
            current_duration = timedelta(
                seconds=TinyTag.get(opus_filename).duration
            )
        except Exception:
            stdout_data, stderr_data = process.communicate()
            print("STDOUT data:", stdout_data)
            print("STDERR data:", stderr_data)
            continue

        try:
            progress_message = progress_message.edit_text(
                progress_message_template.format(
                    opus_filename=opus_filename,
                    current_duration=current_duration,
                    duration=duration,
                )
            )
        except TelegramError:
            stdout_data, stderr_data = process.communicate()
            print("STDOUT data:", stdout_data)
            print("STDERR data:", stderr_data)

    progress_message.delete()


def convert_to_opus(update, info, bitrate=16):
    executable_paths = ["/root/bin/to-opus", '/home/jupi/bin/voice-to-opus']
    # TODO: Volume

    command = [
        "bash",
        list(filter(os.path.isfile, executable_paths))[0],
        OUTPUT_TEMPLATE % info,
        str(bitrate),
    ]

    p = subprocess.Popen(command,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)

    print("Process ID: %s" % p.pid)

    update_conversion_progress(p, update, info)
    # os.system(command)


def clean_up(info):
    os.remove(OUTPUT_TEMPLATE % info)
    os.remove(OPUS_OUTPUT_TEMPLATE % info)


def get_default_bitrate(update):
    try:
        bitrate = db.search(
            Query().user_id == update.message.from_user.id
        )[0]['bitrate']
    except IndexError:
        bitrate = DEFAULT_BITRATE
    return int(bitrate)


def sizeof_fmt(num, suffix='B'):
    for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Y', suffix)


def get_opus(update, context):
    try:
        bitrate = update.message.text.split(' ')[1]
    except IndexError:
        bitrate = get_default_bitrate(update)

    if 'list' in update.message.text:
        update.message.reply_text("We do not allow playlist downloading")
        return

    for info in download_youtube_video(update):
        convert_to_opus(update, info, bitrate)
        opus_file = open(OPUS_OUTPUT_TEMPLATE % info, "rb")
        size = sizeof_fmt(os.path.getsize(opus_file.name))
        info['description'] = info['description'][:500]
        caption = (
            "<b>Title</b>: {title}\n\n"
            "<b>Duration</b>: {1}\n\n"
            "<b>Size</b>: {0}\n\n"
            "<b>Quality/Bitrate</b>: {2} kbps\n\n"
            "<b>Description</b>: {description}\n\n"
            "<b>Channel</b>: {uploader}\n\n"
            "<b>Link</b>: {webpage_url}\n\n"
            "<b>Converted by</b>: @YouTubeOpusBot"
        ).format(size,
                 timedelta(seconds=info['duration']),
                 bitrate,
                 **info)

        update.message.reply_audio(opus_file,
                                   caption=caption,
                                   parse_mode=ParseMode.HTML)
        clean_up(info)


def save_default_bitrate(update, context):
    bitrate = update.message.text.replace('/set_default_bitrate', '').strip()
    try:
        db.remove(Query().user_id == update.message.from_user.id)
        db.insert({
            'user_id': update.message.from_user.id,
            'bitrate': int(bitrate)
        })
        update.message.reply_text("Default bitrate set to %s" % bitrate)
    except ValueError:
        pass
