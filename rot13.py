#!/usr/bin/env python
# -*- coding: utf-8 -*-
import codecs
import logging
from hashlib import md5
from uuid import uuid4

from cache import DictLikeClient
from telegram import (InlineKeyboardButton, InlineKeyboardMarkup,
                      InlineQueryResultArticle, InputTextMessageContent,
                      ParseMode)
from telegram.ext import (CallbackQueryHandler, CommandHandler, Filters,
                          InlineQueryHandler, MessageHandler, Updater)

client = DictLikeClient(('localhost', 11211))


logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO
)

logger = logging.getLogger(__name__)


def common(text, store_rotated=False):
    rotated_text = codecs.encode(text, "rot_13")
    text_to_store = rotated_text if store_rotated else text

    key = md5(text_to_store.encode('utf8')).hexdigest()
    client.set(key, text_to_store)

    redirect_link = "https://telegram.me/rot13bot?start=%s" % key

    keyboard = [
        [InlineKeyboardButton("Rotate", url=redirect_link)],
        [InlineKeyboardButton("Rotate For Everyone", callback_data=key)]
    ]

    return rotated_text, keyboard


def rotate(update, context):
    rotated_text, keyboard = common(update.message.text)
    update.message.reply_text(rotated_text,
                              reply_markup=InlineKeyboardMarkup(keyboard))


def rotate_cache(update, context):
    stripped_text = update.message.text.replace('/start ', '')
    text = client.get(stripped_text).decode('utf-8')
    rotated_text, keyboard = common(text, True)
    update.message.reply_text(text, reply_markup=InlineKeyboardMarkup(keyboard))


def inline_query_handler(update, context):
    """Handle the inline query."""
    rotated_text, keyboard = common(update.inline_query.query)

    results = [
        InlineQueryResultArticle(
            id=uuid4(),  # id
            title=rotated_text,  # title
            reply_markup=InlineKeyboardMarkup(keyboard),
            input_message_content=InputTextMessageContent(
                rotated_text,
                parse_mode=ParseMode.MARKDOWN
            )
        )
    ]

    update.inline_query.answer(results)


def rotate_button(update, context):
    query = update.callback_query
    text = client.get(query.data).decode('utf-8')
    rotated_text, keyboard = common(text, True)
    query.edit_message_text(text=text,
                            reply_markup=InlineKeyboardMarkup(keyboard))


def error_handler(update, context):
    print(context.error)


updater = Updater("267003811:AAFMJbWt-5EZpY5YJdYBIWIb4g5N2bmc4PU",
                  use_context=True)
dp = updater.dispatcher


dp.add_handler(CommandHandler("start", rotate_cache))
dp.add_handler(MessageHandler(Filters.text, rotate))
dp.add_handler(InlineQueryHandler(inline_query_handler))
dp.add_handler(CallbackQueryHandler(rotate_button))
dp.add_error_handler(error_handler)
updater.start_polling()
updater.idle()
