import json
import os

from .functions import load_survey

JSONS_FOLDERS = os.path.abspath(os.path.join(os.path.abspath(__file__),
                                             '../../jsons/'))
load_survey(json.load(open(os.path.join(JSONS_FOLDERS, 'big5-test.json'))))
load_survey(json.load(open(os.path.join(JSONS_FOLDERS, 'big5.json'))))
