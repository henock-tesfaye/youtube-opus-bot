from . import models


def load_survey(survey_dict):
    questions_data = survey_dict.pop("questions")

    try:
        return models.Survey.get(models.Survey == survey_dict.get('uuid'))
    except models.Survey.DoesNotExist:
        pass

    survey = models.Survey.create(**survey_dict)

    dimension_dicts = [{
        "name": dimension_name,
        "survey": survey
    } for dimension_name in survey_dict["dimensions"]]

    models.Dimension.insert_many(dimension_dicts).execute()

    models.Question.insert_many([{
        "survey": survey,
        "dimension": models.Dimension.get(
            models.Dimension.name == question_datum.pop("dimension")
        ),
        "number": int(question_datum["number"]),
        "reverse_scoring": question_datum["type"] == "R",
        "text": question_datum["question"],
    } for question_datum in questions_data]).execute()
    print(survey)
    return survey
