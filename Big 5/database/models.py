#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from uuid import uuid4

import peewee as pw
# from playhouse.sqlite_ext import CSqliteExtDatabase

db = pw.SqliteDatabase("/tmp/persistent.db")
# db = CSqliteExtDatabase(':memory:')
# db = CSqliteExtDatabase('/tmp/persistent.db' % uuid4())


class TimestampMixin(object):
    created_at = pw.DateTimeField(default=datetime.now)


class Survey(TimestampMixin, pw.Model):
    uuid = pw.UUIDField(unique=True)
    name = pw.CharField()
    background = pw.CharField()
    participation = pw.CharField()
    procedure = pw.CharField()
    prefix = pw.CharField()

    class Meta:
        database = db


class Dimension(TimestampMixin, pw.Model):
    name = pw.CharField()
    survey = pw.ForeignKeyField(Survey, backref="dimensions")

    class Meta:
        database = db


class Question(TimestampMixin, pw.Model):
    survey = pw.ForeignKeyField(Survey, backref="questions")
    dimension = pw.ForeignKeyField(Dimension, backref="questions")
    number = pw.IntegerField()
    reverse_scoring = pw.BooleanField(default=False)
    text = pw.CharField()

    class Meta:
        database = db


class User(pw.Model):
    username = pw.CharField(null=True)
    telegram_id = pw.CharField(unique=True)
    first_name = pw.CharField(null=True)
    last_name = pw.CharField(null=True)

    class Meta:
        database = db


class Session(TimestampMixin, pw.Model):
    uuid = pw.UUIDField(unique=True, default=uuid4)
    user = pw.ForeignKeyField(User, backref="responses")
    survey = pw.ForeignKeyField(Survey, backref="sessions")
    finished_at = pw.DateTimeField(null=True)

    class Meta:
        database = db


class Response(TimestampMixin, pw.Model):
    session = pw.ForeignKeyField(Session, backref="responses")
    question = pw.ForeignKeyField(Question, backref="responses")
    choice = pw.IntegerField()

    class Meta:
        database = db


db.create_tables([Survey, Dimension, Question, User, Session, Response])
