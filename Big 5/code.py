#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import operator as op
from collections import defaultdict
from datetime import datetime

from database import models
from telegram import (InlineKeyboardButton, InlineKeyboardMarkup,
                      KeyboardButton, ParseMode, ReplyKeyboardMarkup)
from telegram.ext import (CallbackQueryHandler, CommandHandler, Filters,
                          MessageHandler, Updater)

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO
)

logger = logging.getLogger(__name__)
survey = models.Survey.get(
    models.Survey.uuid == "d713b43100044ad58e4dcc4418c361ea"
)

session = None

##############################################################################

VALUE_REVERSER_DICT = dict(zip(range(1, 6), range(5, 0, -1)))


def get_current_question():
    global session
    try:
        number_of_responses = session.responses.count()
    except IndexError:
        number_of_responses = 0

    return survey.questions[number_of_responses]


def calculate_result(update, context):
    result = defaultdict(int)

    for dimension in session.survey.dimensions:
        choices = (models.Response
            .select(models.Response, models.Question)
            .join(models.Question)
            .where(models.Response.session == session,
                   models.Question.dimension == dimension))
        print(list(choices))
        result[dimension.name] = sum(map(op.attrgetter('choice'), choices), 0)

    message_per_dimension = map("<b>{}</b>: {}".format,
                                result.keys(),
                                result.values())
    message = '\n\n'.join(message_per_dimension)
    update.message.reply_text(
        message,
        reply_markup=InlineKeyboardMarkup([[
            InlineKeyboardButton('Restart', callback_data='restart')
        ]]),
        parse_mode=ParseMode.HTML)


def get_next_question(update, context):
    try:
        question = get_current_question()
    except IndexError:
        return calculate_result(update, context)

    question_representation = "{}/{}. {}{}".format(
        session.responses.count() + 1,
        survey.questions.count(),
        survey.prefix,
        question.text,
    )

    update.message.reply_text(
        question_representation,
        reply_markup=ReplyKeyboardMarkup([
            ['1', '2'],
            ['3', '4'],
            ['5']
        ], one_time_keyboard=True)
    )


def accept_reply(update, context):
    global session
    current_question = get_current_question()
    dimension = current_question.dimension
    value = int(update.message.text)
    if current_question.reverse_scoring:
        effective_value = VALUE_REVERSER_DICT[value]
    else:
        effective_value = value

    models.Response.create(
        session=session,
        question=current_question,
        choice=effective_value
    )
    get_next_question(update, context)


def start_session(update, context):
    global session
    user, created = models.User.get_or_create(
        telegram_id=update.message.from_user.id,
        defaults={
            'username': update.message.from_user.username,
            'first_name': update.message.from_user.first_name,
            'last_name': update.message.from_user.last_name,
        }
    )
    session = models.Session.create(user=user, survey=survey)


def introduce(update, context):
    update.message.reply_text("Welcome to <b>the Big 5</b> personality test",
                              parse_mode=ParseMode.HTML)

    update.message.reply_text(
        "<b>Background</b>\n\n %s" % survey.background,
        parse_mode=ParseMode.HTML)

    update.message.reply_text(
        "<b>Procedure</b>\n\n %s" % survey.procedure,
        parse_mode=ParseMode.HTML)

    update.message.reply_text(
        "<b>Participation</b>\n\n %s" % survey.participation,
        reply_markup=InlineKeyboardMarkup([[
            InlineKeyboardButton('I understand.\nLet us start',
                                 callback_data="begin")
        ]]),
        parse_mode=ParseMode.HTML)


def begin_test(update, context):
    update.message = update.callback_query.message
    start_session(update, context)
    get_next_question(update, context)


if __name__ == "__main__":
    updater = Updater('829465295:AAHSHqG7jRsA-yNRPlvQFiZ5n68eJrOXUuo',
                      use_context=True)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', introduce))
    dp.add_handler(CallbackQueryHandler(begin_test))
    dp.add_handler(MessageHandler(Filters.regex(r'[1-5]'), accept_reply))
    updater.start_polling()
    updater.idle()
