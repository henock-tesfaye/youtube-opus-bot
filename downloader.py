#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import mimetypes
import os
import re
# import tempfile
import urllib.parse
from uuid import uuid4

import requests

from telegram import InlineQueryResultArticle
from telegram.ext import Filters, InlineQueryHandler, MessageHandler, Updater

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO
)

logger = logging.getLogger(__name__)


def downloader(url):
    print("URL: ", url)
    response = requests.get(url)
    quoted_filename = os.path.basename(urllib.parse.urlparse(url).path)
    filename = urllib.parse.unquote_plus(quoted_filename)
    print("Filename: ", filename)

    # content_type = response.headers['Content-Type']
    if not re.search(r'\.\w{2,4}$', filename):
        content_type, _ = mimetypes.guess_type(filename, strict=True)
        print("Content Type: ", content_type)
        extension = mimetypes.guess_extension(content_type)
        print("Extension: ", extension)
        temp = open("/tmp/%s.%s" % (filename, extension), "wb")
        # tempfile.NamedTemporaryFile(prefix=filename + "-", suffix=extension)
    else:
        temp = open("/tmp/%s" % filename, "wb")
        content_type = filename.split('.')[-1]

    temp.write(response.content)
    temp.flush()

    if content_type.startswith("image"):
        if "svg" not in content_type:
            file_type = "photo"
        else:
            file_type = 'document'
    elif content_type.startswith("audio"):
        file_type = "audio"
    elif content_type.startswith("video"):
        file_type = "video"
    else:
        file_type = 'document'

    # temp.close()
    return open(temp.name, "rb"), file_type


def link_handler(update, context):
    file_, file_type = downloader(update.message.text)
    getattr(update.message, 'reply_%s' % file_type)(file_)


def inline_query_handler(update, context):
    """Handle the inline query."""
    file_, file_type = downloader(update.inline_query.query)
    results = [
        InlineQueryResultArticle(
            uuid4(),  # id
            "Click here",  # title
            # input_message_content=InputMediaDocument(file_)
        )
    ]

    update.inline_query.answer(results)


def error(update, context):
    print(context.error)


updater = Updater("989798262:AAFiGRLfj_5omad7w_yUnbHWhjTgjPnI5vc",
                  use_context=True)
dp = updater.dispatcher
dp.add_handler(MessageHandler(Filters.text, link_handler))
dp.add_handler(InlineQueryHandler(inline_query_handler))
dp.add_error_handler(error)

updater.start_polling()
updater.idle()
